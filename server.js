const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000
const fs = require('fs')

app.use(express.json());
app.listen(PORT, () => console.log(`Server is listening on port ${PORT}...`))
const path = "réponses.json";


function readFileAsPromised(file, encoding) {
    return new Promise( (resolve, reject) => {
        fs.readFile(file, encoding, (err, data) => {
            if (err) reject(err);
            else resolve(data);
        });
    });
}

function writeFileAsPromised(file, data) {
    return new Promise( (resolve, reject) => {
        fs.writeFile(file, data, (err) => {
            if (err) reject(err);
            else resolve();
        });
    });
}

const promiseAsync = readFileAsPromised(path, 'utf8');

( async() => {
    let data;
    try {
        data = await promiseAsync;
    } catch (err) {
        console.error("Echec de lecture du fichier:", err);
        process.exit();
    }

    let dataJSON = JSON.parse(data);

    app.get('/', function (req, res) {
        res.send('Hello World')
    })

    app.get('/hello', function (req, res) {
        if (req.query.nom) {
            res.send('Bonjour, '+ req.query.nom + ' !')
        } else {
            res.send('Quel est votre nom ?')
        }
    })

    app.post('/chat', async function (req, res) {

        let requestUser = req.body.msg;

        if ( requestUser.search(' = ') == -1 ) {
            if ( dataJSON[requestUser] != undefined ) {

                if (requestUser == "ville" || requestUser == "météo") {
                    res.send( dataJSON[requestUser] )
                } else {
                    res.send( requestUser + ': ' + dataJSON[requestUser])
                }

            } else if ( dataJSON[requestUser] == undefined ) {
                res.send( 'Je ne connais pas ' + requestUser + '...' )
            }
        } else {
            let arrayNewData = req.body.msg.split(' = ');
            dataJSON[arrayNewData[0]] = arrayNewData[1];

            try {
                await writeFileAsPromised(path, JSON.stringify(dataJSON))
                res.send("Merci pour cette information !");
            } catch (err) {
                console.error(err);
                res.send("Erreur... Réessayez plus tard.");
            }
        }
    })
})();
