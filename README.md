## Chatbot Node.js

## Quel est son but ?

Affichage du texte 'Hello World !' lors d'une requête GET.

## Installation

Création d'un repository git puis clonage du dossier sur sa machine locale --> $ git clone
Installation du module --> $ npm install
Lancement du serveur --> $ npm start

## Phase de test

Commande pour lancer la requête --> $ curl http://localhost:3000/
Texte retourné : 'Hello World !'

## Ex 4/5
Request : 
$ curl -X POST --header "Content-Type: application/json" --data "{\"msg\":\"demain\"}" http://localhost:3000/chat répondra “Je ne connais pas demain…”
$ curl -X POST --header "Content-Type: application/json" --data "{\"msg\":\"demain = Mercredi\"}" http://localhost:3000/chat répondra “Merci pour cette information !”
$ curl -X POST --header "Content-Type: application/json" --data "{\"msg\":\"demain\"}" http://localhost:3000/chat répondra “demain: Mercredi” (y compris après redémarrage du serveur)
